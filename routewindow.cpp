#include "routewindow.h"

RouteWindow::RouteWindow(socket_t & socket, int routenum) :
    m_socket(socket),
    m_InManualMode(false)
{
    m_LCD_INFO.routenum = routenum;
    TXToController();
    this->resize(SCREEN_WIDTH, SCREEN_HEIGHT);
    m_layout = new QGridLayout(this);
    this->setLayout(m_layout);
    this->m_NextStop = new QLabel("Waiting For Stop");
    this->m_CurrentRoute = new QLabel("Waiting For Route");
    this->m_SkipStop = new QPushButton("Skip");
    m_SkipStop->setFixedHeight(90);
    this->m_Requested = new QPushButton("Request Stop");
    m_Requested->setFixedHeight(90);
    this->m_RequestStatus = new QLabel;
    this->m_EnterManual = new QPushButton("Manual Mode");
    m_EnterManual->setFixedHeight(90);
    this->m_AnnounceStop = new QPushButton("Announce Stop");
    m_AnnounceStop->setFixedHeight(90);
    this->m_TriggerNextStop = new QPushButton("Next Stop");
    m_TriggerNextStop->setFixedHeight(90);
    this->m_polling = new QTimer();
    printf("Built window\n");
    m_polling->setInterval(250);
    m_polling->start();
    m_layout->addWidget(m_CurrentRoute, 0, 0);
    m_layout->addWidget(m_NextStop, 1, 0);
    m_layout->addWidget(m_EnterManual, 3, 0);
    m_layout->addWidget(m_RequestStatus, 3, 1);
    m_layout->addWidget(m_SkipStop, 5, 0);
    m_layout->addWidget(m_Requested, 5, 1);
    printf("Added Widgets\n");
    connect(m_Requested, &QPushButton::clicked, this, &RouteWindow::RequestStop);
    connect(m_polling, &QTimer::timeout, this, &RouteWindow::UpdateData);
    connect(m_SkipStop, &QPushButton::clicked, this, &RouteWindow::SkipStop);
    connect(m_EnterManual, &QPushButton::clicked, this, &RouteWindow::ToggleManualMode);
    connect(m_TriggerNextStop, &QPushButton::clicked, this, &RouteWindow::SkipStop);
    connect(m_AnnounceStop, &QPushButton::clicked, this, &RouteWindow::AnnounceStop);
    showFullScreen();
}

void RouteWindow::TXToController()
{
    char * StrToTx = m_LCD_INFO.createstring();
    printf("Sending %s\n", StrToTx);
    m_socket.send_msg((void *)StrToTx, strlen(StrToTx) + 1);
    delete StrToTx;
}

void RouteWindow::RequestStop()
{
    m_LCD_INFO.announce = false;
    m_LCD_INFO.skipstop = false;
    m_LCD_INFO.requeststop = !m_LCD_INFO.requeststop;
    TXToController();
}

void RouteWindow::ToggleManualMode()
{
    if(m_LCD_INFO.manual)
    {
        m_LCD_INFO.manual = false;

        //Disable Manual Mode
        m_layout->removeWidget(m_TriggerNextStop);
        m_layout->removeWidget(m_AnnounceStop);
        m_AnnounceStop->hide();
        m_TriggerNextStop->hide();

    }
    else
    {
        m_LCD_INFO.manual = true;
        //Enable Manual Mode
        m_layout->addWidget(m_TriggerNextStop,4, 0);
        m_layout->addWidget(m_AnnounceStop, 4, 1);
        m_AnnounceStop->show();
        m_TriggerNextStop->show();
    }
    m_LCD_INFO.announce = false;
    m_LCD_INFO.skipstop = false;
    m_LCD_INFO.requeststop =false;
    TXToController();
}

void RouteWindow::SkipStop()
{
    m_LCD_INFO.announce = false;
    m_LCD_INFO.requeststop = false;
    m_LCD_INFO.skipstop = true;
    TXToController();
    m_LCD_INFO.skipstop = false;
}
void RouteWindow::UpdateData()
{
    try
    {
        if(m_socket.data_avail())
        {
            char * data = (char *)m_socket.receive_msg(5000);
            printf("Recved: %s\n", data);
            m_CurrentRoute->setText(strtok(data, ":"));
            char * StopStr = strtok(nullptr, ":");
            char * StopReq = strtok(nullptr, ":");
            Stop * currStop = new Stop(StopStr);
            m_NextStop->setText(currStop->Name.c_str());

            if(strcmp(StopReq, "1") == 0)
            {
                m_RequestStatus->setText("Stop Requested");
                m_LCD_INFO.requeststop = true;
            }
            else
            {
                m_RequestStatus->setText("");
                m_LCD_INFO.requeststop = false;
            }
            delete currStop;
            delete data;
        }
    }
    catch (Exception e)
    {
        printf("Error: %s", e.GetString());
    }
}

void RouteWindow::AnnounceStop()
{
    m_LCD_INFO.requeststop = false;
    m_LCD_INFO.skipstop = false;
    m_LCD_INFO.announce = true;
    TXToController();
    m_LCD_INFO.announce = false;

}
void RouteWindow::CheckSocketAndTx()
{

}

RouteWindow::~RouteWindow()
{
    delete m_layout;
    delete m_NextStop;
    delete m_SkipStop;
    delete m_Requested;
    delete m_CurrentRoute;
}
