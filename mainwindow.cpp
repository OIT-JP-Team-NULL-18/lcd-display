#include "mainwindow.h"


mainWindow::mainWindow(QWidget * parent) :
    QWidget(parent)
{
    this->resize(SCREEN_WIDTH, SCREEN_HEIGHT);
    m_SelectRoute = new QPushButton("Select Route");
    m_layout = new QGridLayout(this);
    m_layout->addWidget(m_SelectRoute);
    m_Routes = new QComboBox;
    m_StartBtApp = new QPushButton("Start Bluetooth");
    m_layout->addWidget(m_StartBtApp);
    m_Routes->addItem("Route 1");
    m_Routes->addItem("Route 2");
    m_Routes->addItem("Route 3");
    m_Routes->setFixedHeight(90);
    m_SelectRoute->setFixedHeight(90);
    m_layout->addWidget(m_Routes);
    m_error_label = new QLabel;
    m_layout->addWidget(m_error_label);
    try {
        m_controller.setup_client(mysettings.get_lcd_socket().c_str());
    } catch (Exception e) {
        m_error_label->setText(e.GetString());
    }
    m_RouteWindow = nullptr;
    connect(m_SelectRoute, SIGNAL (clicked()), this, SLOT(OpenRouteWindow()));
    connect(m_StartBtApp, SIGNAL (clicked()), this, SLOT(OpenBtApp()));
    showFullScreen();
}
void mainWindow::OpenBtApp()
{
    /*/home/pi/Desktop/BluetoothBackend/bluetooth*/
    std::system("xterm -e /home/pi/Desktop/BluetoothBackend/bluetooth");
}
void mainWindow::OpenRouteWindow()
{
    QString CurrRoute = m_Routes->currentText();
    QStringList splits = CurrRoute.split(' ');
    std::string RoutNum = splits[1].toUtf8().constData();
    int routenum = atoi(RoutNum.c_str());
    m_RouteWindow = new RouteWindow(m_controller, routenum);
    m_RouteWindow->show();
}
mainWindow::~mainWindow()
{
    delete m_SelectRoute;
    delete m_layout;
    delete m_Routes;
    delete m_error_label;
    delete m_StartBtApp;
}
