#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    mainWindow myWindow;
    myWindow.show();

    return a.exec();
}
