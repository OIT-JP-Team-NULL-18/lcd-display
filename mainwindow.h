#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtCore>
#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <string>
#include <string.h>
#include "common/common.h"
#include "routewindow.h"
class mainWindow : public QWidget
{
    Q_OBJECT
public:
    mainWindow(QWidget * parent = nullptr);
    ~mainWindow();
private slots:
    void OpenRouteWindow();
    void OpenBtApp();
private:
    QComboBox * m_Routes;
    QPushButton * m_SelectRoute;
    QPushButton * m_StartBtApp;
    QGridLayout * m_layout;
    QLabel * m_error_label;
    socket_t m_controller;
    RouteWindow * m_RouteWindow;
    settings mysettings;
};

#endif // MAINWINDOW_H
