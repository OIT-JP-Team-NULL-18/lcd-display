#ifndef ROUTEWINDOW_H
#define ROUTEWINDOW_H
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QTimer>
#include <stdlib.h>
#include "common/common.h"

class RouteWindow : public QWidget
{
public:
    RouteWindow(socket_t & socket, int routenum);
    void TXToController();
    ~RouteWindow();
private slots:
    void RequestStop();
    void ToggleManualMode();
    void SkipStop();
    void CheckSocketAndTx();
    void UpdateData();
    void AnnounceStop();
private:
    QLabel * m_CurrentRoute;
    QLabel * m_NextStop;
    QLabel * m_Status;
    QLabel * m_RequestStatus;
    QPushButton * m_EnterManual;
    QPushButton * m_SkipStop;
    QPushButton * m_Requested;
    QPushButton * m_TriggerNextStop;
    QPushButton * m_AnnounceStop;
    socket_t & m_socket;
    QGridLayout * m_layout;
    QTimer * m_polling;
    LCD m_LCD_INFO;
    bool m_InManualMode;
};

#endif // ROUTEWINDOW_H
